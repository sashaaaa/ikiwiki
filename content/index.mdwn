# Welcome!

Hi, I'm Sasha, a student of information processing science at the
University of Oulu with interests in free software, the demoscene and
the decentralised Web. I've dabbled in various programming languages
such as C/C++ and Java but my main interests at the moment are in D,
Raku and Lisp!

In my free time, I enjoy playing the bass, baking tasty treats, riding
on my bike, or going out and taking some photos, and I love music!

With this site I'm hoping to share what I'm up to as well as hopefully
some useful information.

<a href="https://www.perl.org" class="aimg"> <img
  src="images/powered_by_perl.webp" alt="Powered by Perl" width="122"
  height="55"> </a> <a href="https://www.ikiwiki.info" class="aimg" >
  <img src="images/ikiwiki_button.webp" alt="ikiwiki" width="122"
  height="53"> </a> <a href="https://www.gnu.org/software/emacs/"
  class="aimg" > <img src="images/emacs.webp" alt="GNU Emacs"
  width="122" height="53"> </a>
