[[!sidebar content="""
[[!calendar type=month month=08 year=2025 pages="blog/posts/* and !*/Discussion"]]
"""]]

[[!inline pages="creation_month(08) and creation_year(2025) and blog/posts/* and !*/Discussion" show=0 feeds=no reverse=yes]]
